#!/bin/bash

for i in $@; do
    case "$i" in
        -h|--help)
            echo "usage: $0 (rootdir=<directory>)"
            exit 0;;
        rootdir=*)
            rootdir=${i#*=};;
    esac
done

install -v -Dm755 live.sh $rootdir/bin/live

install -v -Dm644 live.cfg $rootdir/etc/live.conf

install -v -Dm644 grub.cfg $rootdir/share/live/grub.cfg
install -v -Dm644 isolinux.cfg $rootdir/share/live/isolinux.cfg
install -v -m644 install.txt $rootdir/share/live/install.txt
